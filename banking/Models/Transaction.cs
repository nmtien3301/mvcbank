﻿using System;
using System.Collections.Generic;

namespace banking.Models;

public partial class Transaction
{
    public int TransactionsId { get; set; }

    public int EmployeeId { get; set; }

    public int CustomerId { get; set; }

    public string Name { get; set; }

    public virtual Customer Customer { get; set; }

    public virtual Employee Employee { get; set; }

    public virtual ICollection<Log> Logs { get; set; } = new List<Log>();
}
