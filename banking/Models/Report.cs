﻿using System;
using System.Collections.Generic;

namespace banking.Models;

public partial class Report
{
    public int ReportId { get; set; }

    public int AccountId { get; set; }

    public int LogsId { get; set; }

    public int TransactionalId { get; set; }

    public string Reportname { get; set; }

    public DateTime Reportdate { get; set; }

    public virtual Account Account { get; set; }
}
