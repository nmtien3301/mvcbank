﻿using System;
using System.Collections.Generic;

namespace banking.Models;

public partial class Employee
{
    public int EmployeeId { get; set; }

    public string Firstname { get; set; }

    public string Lastname { get; set; }

    public string ContactAndAddress { get; set; }

    public string Username { get; set; }

    public string Password { get; set; }

    public virtual ICollection<Transaction> Transactions { get; set; } = new List<Transaction>();
}
