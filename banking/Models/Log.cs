﻿using System;
using System.Collections.Generic;

namespace banking.Models;

public partial class Log
{
    public int LogsId { get; set; }

    public int TransactionsId { get; set; }

    public DateTime LoginDate { get; set; }

    public TimeOnly LoginTime { get; set; }

    public virtual Transaction? Transactions { get; set; }
}
