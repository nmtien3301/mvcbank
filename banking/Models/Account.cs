﻿using System;
using System.Collections.Generic;

namespace banking.Models;

public partial class Account
{
    public int AccountsId { get; set; }

    public int CustomerId { get; set; }

    public string AccountName { get; set; }

    public virtual Customer? Customer { get; set; }

    public virtual ICollection<Report> Reports { get; set; } = new List<Report>();
}
