﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace banking.Models;

public partial class BankTtContext : DbContext
{
    public BankTtContext()
    {
    }

    public BankTtContext(DbContextOptions<BankTtContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Account> Accounts { get; set; }

    public virtual DbSet<Customer> Customers { get; set; }

    public virtual DbSet<Employee> Employees { get; set; }

    public virtual DbSet<Log> Logs { get; set; }

    public virtual DbSet<Report> Reports { get; set; }

    public virtual DbSet<Transaction> Transactions { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see https://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=MSI\\SQLEXPRESS;Database=BankTT;Trusted_Connection=True;TrustServerCertificate=true;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Account>(entity =>
        {
            entity.HasKey(e => e.AccountsId).HasName("PK__Accounts__BB3A1BB6D20949F2");

            entity.Property(e => e.AccountsId).HasColumnName("Accounts ID");
            entity.Property(e => e.AccountName)
                .HasMaxLength(255)
                .HasColumnName("Account name");
            entity.Property(e => e.CustomerId).HasColumnName("Customer ID");

            entity.HasOne(d => d.Customer).WithMany(p => p.Accounts)
                .HasForeignKey(d => d.CustomerId)
                .HasConstraintName("FK_Accounts_Customer");
        });

        modelBuilder.Entity<Customer>(entity =>
        {
            entity.HasKey(e => e.CustomerId).HasName("PK__Customer__9CF1F3A875BFA385");

            entity.ToTable("Customer");

            entity.Property(e => e.CustomerId).HasColumnName("Customer ID");
            entity.Property(e => e.ContactAndAddress)
                .HasMaxLength(50)
                .HasColumnName("Contact and address");
            entity.Property(e => e.Firstname).HasMaxLength(30);
            entity.Property(e => e.Lastname).HasMaxLength(20);
            entity.Property(e => e.Password).HasMaxLength(255);
            entity.Property(e => e.Username).HasMaxLength(50);
        });

        modelBuilder.Entity<Employee>(entity =>
        {
            entity.HasKey(e => e.EmployeeId).HasName("PK__Employee__682EC9E44383622D");

            entity.Property(e => e.EmployeeId).HasColumnName("Employee ID");
            entity.Property(e => e.ContactAndAddress)
                .HasMaxLength(50)
                .HasColumnName("Contact and address");
            entity.Property(e => e.Firstname).HasMaxLength(50);
            entity.Property(e => e.Lastname).HasMaxLength(20);
            entity.Property(e => e.Password).HasMaxLength(255);
            entity.Property(e => e.Username).HasMaxLength(50);
        });

        modelBuilder.Entity<Log>(entity =>
        {
            entity.HasKey(e => e.LogsId).HasName("PK__Logs__9218008D9B716F41");

            entity.Property(e => e.LogsId).HasColumnName("Logs ID");
            entity.Property(e => e.LoginDate)
                .HasColumnType("datetime")
                .HasColumnName("Login date");
            entity.Property(e => e.LoginTime).HasColumnName("Login time");
            entity.Property(e => e.TransactionsId).HasColumnName("Transactions ID");

            entity.HasOne(d => d.Transactions).WithMany(p => p.Logs)
                .HasForeignKey(d => d.TransactionsId)
                .HasConstraintName("FK_Logs_Transactions");
        });

        modelBuilder.Entity<Report>(entity =>
        {
            entity.HasKey(e => e.ReportId).HasName("PK__Reports__D5BD48E515CC451C");

            entity.Property(e => e.ReportId).HasColumnName("ReportID");
            entity.Property(e => e.AccountId).HasColumnName("Account ID");
            entity.Property(e => e.LogsId).HasColumnName("Logs ID");
            entity.Property(e => e.Reportdate).HasColumnType("datetime");
            entity.Property(e => e.Reportname).HasMaxLength(255);
            entity.Property(e => e.TransactionalId).HasColumnName("Transactional ID");

            entity.HasOne(d => d.Account).WithMany(p => p.Reports)
                .HasForeignKey(d => d.AccountId)
                .HasConstraintName("FK_Reports_Accounts");
        });

        modelBuilder.Entity<Transaction>(entity =>
        {
            entity.HasKey(e => e.TransactionsId).HasName("PK__Transact__BB03F23D58E99B2B");

            entity.Property(e => e.TransactionsId).HasColumnName("Transactions ID");
            entity.Property(e => e.CustomerId).HasColumnName("Customer ID");
            entity.Property(e => e.EmployeeId).HasColumnName("Employee ID");
            entity.Property(e => e.Name).HasMaxLength(255);

            entity.HasOne(d => d.Customer).WithMany(p => p.Transactions)
                .HasForeignKey(d => d.CustomerId)
                .HasConstraintName("FK_Transactions_Customer");

            entity.HasOne(d => d.Employee).WithMany(p => p.Transactions)
                .HasForeignKey(d => d.EmployeeId)
                .HasConstraintName("FK_Transactions_Employees");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
