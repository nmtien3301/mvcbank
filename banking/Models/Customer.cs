﻿using System;
using System.Collections.Generic;

namespace banking.Models;

public partial class Customer
{
    public int CustomerId { get; set; }

    public string Firstname { get; set; }

    public string Lastname { get; set; }

    public string ContactAndAddress { get; set; }

    public string Username { get; set; }

    public string Password { get; set; }

    public virtual ICollection<Account> Accounts { get; set; } = new List<Account>();

    public virtual ICollection<Transaction> Transactions { get; set; } = new List<Transaction>();
}
